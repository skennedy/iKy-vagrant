#!/usr/bin/env bash
while read -r name; do
    uuid=$(echo $name |awk '{print$2}' |sed -e 's/.*{\([^}]\+\)}.*/\1/g')
    echo "Unregistering VM '$(echo $name)'..."
    vboxmanage unregistervm "$uuid" --delete # &> /dev/null
done < <(VBoxManage list vms | grep iKy)
