# Vagrant Project iKy

## PROJECT
Visit the Gitlab Page of the [Project](https://kennbroorg.gitlab.io/ikyweb/)
The original project repo is [here](https://gitlab.com/kennbroorg/iKy)

This repo has been created to easy install the tool.

### Install the following first:
 - [vagrant] (http://www.vagrantup.com/downloads.html)
 - [virtualbox] (https://www.virtualbox.org/wiki/Downloads)

### Clone this repo and execute vagrant
```shell
git clone https://gitlab.com/kennbroorg/iKy-vagrant.git
cd iKy-vagrant/
vagrant up
```
This step takes a long time, be patient
Sometimes the initial provision times out at the 'gathering facts' stage because vagrant VM is not fully up yet; if you see an error that says something about an ssh timeout, simply do 'vagrant provision' to resume the provisioning

### On the host computer:
 Open your browser and go to [http://127.0.0.1:3000](http://127.0.0.1:3000) 

#### Config API Keys
For now please load the apiKeys of [fullcontact](https://support.fullcontact.com/hc/en-us/articles/115003415888-Getting-Started-FullContact-v2-APIs) and [twitter](https://developer.twitter.com/en/docs/basics/authentication/guides/access-tokens.html) through the **API Keys** option in the frontend to get more information

### On the vagrant VM:
To access the vagrant machine 
```shell
vagrant ssh
```
Logs are stored as follows (on the VM):
 - redis server: /home/vagrant/log/redis.log
 - celery server: /home/vagrant/log/celery.log
 - app server: /home/vagrant/log/app.log
 - gulp server: /home/vagrant/log/gulp.log
