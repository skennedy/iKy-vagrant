ln -s /home/vagrant/.local/bin/celery /usr/local/bin/celery
cd ~
mkdir log
mkdir .config
echo
echo "Updating..."
sudo apt-get update
sudo apt-get install -y build-essential
echo 

echo "Installing redis..."
wget -q http://download.redis.io/redis-stable.tar.gz -O redis-stable.tar.gz
tar xvfz redis-stable.tar.gz
cd redis-stable
make
sudo make install
echo 
echo "Execute redis-server..."
redis-server > $HOME/log/redis.log 2>&1 &
cd /home/vagrant
pwd
echo 

sudo apt-get install -y curl python-software-properties
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
echo
echo "Installing nodejs..."
sudo apt-get install -y nodejs
node -v
npm -v 
echo

echo "Installing Bower..."
sudo npm install -g bower
sudo chown -R vagrant .config
echo

echo "Cloning Repo ${REPO_URL_IKY}"
# su vagrant -c 'git clone https://gitlab.com/kennbroorg/iKy.git'
git clone https://gitlab.com/kennbroorg/iKy.git
cd iKy/
pwd
echo

echo "Installing celery and package from pip..."
sudo apt-get install -qy python-pip python-celery
echo "Installing pip and requirements..."
pip2 install -U pip
pip2 install -r requirements.txt
echo 

echo "Execute celery..."
cd backend 
pwd
./celery.sh >$HOME/log/celery.log 2>&1 &
echo "Execute app..."
python app.py -i 0.0.0.0 >$HOME/log/app.log 2>&1 &
# su vagrant -c 'python app.py &'
echo

echo "Installing package from frontend..."
cd ..
cd frontend
pwd
npm install
echo "Installing gulp..."
sudo npm install -g gulp
sleep 10
echo "Bower Install"
bower install
echo "Running gulp serve"
gulp serve >$HOME/log/gulp.log 2>&1 &
sleep 20
tail -n 50 $HOME/log/gulp.log

# Make some checks
/vagrant/checks.sh

if [ $? -ne 0 ]; then
    bower install
fi

echo "Retry running gulp serve"
gulp serve >$HOME/log/gulp.log 2>&1 &
sleep 20
tail -n 50 $HOME/log/gulp.log

# Make some checks
/vagrant/checks.sh
